﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace CheckArrayIntersect
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string[] Foo_Old = new[] { "test0", "test", "test3" };
                string[] Foo_New = new[] { "test1", "test2", "test4", "test5" };
                List<string> comparedList = Foo_New.Intersect(Foo_Old).ToList();
                int count = comparedList.Count;

                string pathValue1 = @"https://google.co.in/Folder1.1/ab.c.pptx";
                string pathValue2 = @"https://google.co.in/Folder1";

                string pathVal1Ext = System.IO.Path.GetExtension(pathValue1).Substring(1);
                int pathVal1ExtLength = pathVal1Ext.Length;

                string pathVal2Ext = System.IO.Path.GetExtension(pathValue2);
                int pathVal2ExtLength = pathVal2Ext.Length;

                if (pathVal1Ext.Contains(pathVal2Ext))
                {
                    Console.WriteLine("Matching....");
                }

                string date = "24/06/2016";
                DateTime dtdocumentDate = DateTime.ParseExact(date, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                string enGBdocumentDate = dtdocumentDate.ToString("MM/dd/yyyy");

                string sqlConnectionString = @"Server=tcp:literacontenthub.database.windows.net,1433;Initial Catalog=ContentHub_AuditDB;Persist Security Info=False;User ID=contenthubadmin;Password=Admin@123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                string script = File.ReadAllText(@"C:\Users\mmraval\Documents\SQL Server Management Studio\Code Snippets\SQL\My Code Snippets\SQLQuery1.sql");
                SqlConnection conn = new SqlConnection(sqlConnectionString);
                Server server = new Server(new ServerConnection(conn));
                server.ConnectionContext.ExecuteNonQuery(script);
            }
            catch
            {
            }
        }
    }
}
