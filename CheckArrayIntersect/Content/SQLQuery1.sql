-- =========================================
-- Create table template SQL Azure Database 
-- =========================================

--IF OBJECT_ID('<schema_name, sysname, dbo>.<table_name, sysname, sample_table>', 'U') IS NOT NULL
--  DROP TABLE <schema_name, sysname, dbo>.<table_name, sysname, sample_table>
--GO

IF OBJECT_ID('dbo.AuditReport', 'U') IS NULL
CREATE TABLE dbo.AuditReport
(
	ID int NOT NULL primary key,
	Name nvarchar(50) NULL,
	Address nvarchar(50) NULL
)

GO
